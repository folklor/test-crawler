package com.epam.test.crawler;

import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.RecursiveAction;

public class CrawlerRecursiveAction extends RecursiveAction {

    private final String targetUrl;

    private final String rootUrl;

    private final Set<String> urls;

    private final int URLS_NUMBER_LIMIT;

    private final int DEPTH_LIMIT;

    /**
     * Constructor which should be used once to create instance for ForkJoinPool.
     *
     * @param rootUrl root url;
     */
    CrawlerRecursiveAction(String rootUrl, int urlsNumberLimit, int depthLimit) {
        this.rootUrl = rootUrl;
        this.targetUrl = rootUrl;
        this.urls = Collections.synchronizedSet(new HashSet<>());
        this.urls.add(this.targetUrl);
        this.URLS_NUMBER_LIMIT = urlsNumberLimit;
        this.DEPTH_LIMIT = depthLimit;
        System.out.printf("Started scanning of %s with depth: %d and urls number limit: %d\r\n",
                this.rootUrl, this.DEPTH_LIMIT, this.URLS_NUMBER_LIMIT);
    }

    /**
     * Constructor used just for internal recursive calls.
     *
     * @param rootUrl         root url;
     * @param targetUrl       target url;
     * @param urls            urls set for prevent duplications;
     * @param urlsNumberLimit urls number limit;
     * @param depthLimit      depth limit;
     */
    private CrawlerRecursiveAction(String rootUrl, String targetUrl, Set<String> urls, int urlsNumberLimit, int depthLimit) {
        this.rootUrl = rootUrl;
        this.targetUrl = targetUrl;
        this.urls = urls;
        this.URLS_NUMBER_LIMIT = urlsNumberLimit;
        this.DEPTH_LIMIT = depthLimit;
    }

    @Override
    protected void compute() {
        List<CrawlerRecursiveAction> subActions = new LinkedList<>();
        Document doc;
        try {
            doc = Jsoup.connect(targetUrl).get();
            String title = doc.title();
            System.out.printf("Title: %s \r\nURL: %s\r\n", title, targetUrl);
            if (DEPTH_LIMIT > 0) {
                Elements links = doc.select("a[href]");
                synchronized (urls) {
                    for (Element link : links) {
                        String absUrl = link.attr("abs:href");
                        if (absUrl.startsWith(rootUrl)
                                && !urls.contains(absUrl)
                                && (URLS_NUMBER_LIMIT == 0 || urls.size() < URLS_NUMBER_LIMIT)) {
                            urls.add(absUrl);
                            CrawlerRecursiveAction subAction = new CrawlerRecursiveAction(rootUrl, absUrl, urls,
                                    URLS_NUMBER_LIMIT, DEPTH_LIMIT - 1);
                            subAction.fork();
                            subActions.add(subAction);
                        }
                    }
                }
                for (CrawlerRecursiveAction subAction : subActions) {
                    subAction.join();
                }
            }

        } catch (UnsupportedMimeTypeException e) {
            System.out.printf("Mime type: %s \r\nURL: %s\r\n", e.getMimeType(), targetUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
