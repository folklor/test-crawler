package com.epam.test.crawler;

import java.util.concurrent.ForkJoinPool;

public class Main {
    public static void main(String[] args) {
        new ForkJoinPool().invoke(new CrawlerRecursiveAction("https://www.epam.com/", 12, 2));
    }
}
